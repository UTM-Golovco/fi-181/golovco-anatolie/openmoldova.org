# Docker
## Presupunând o instalare proaspătă a Ubuntu 20.04 
   NOTA: testat și pe 22.04

### **Instalați Docker și docker-compose**

1. [Install Docker](https://docs.docker.com/get-docker/)
2. [Install Docker Compose](https://docs.docker.com/compose/).

Instalarea Docker:
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce
```

Verificare status Daemon Docker:

```bash
sudo systemctl status docker
```

Instalare Docker-Compose:

```bash
sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
```

Configurații suplimentare:

```bash
# Creați grup docker dacă nu există
sudo groupadd docker
# Adăugați utilizatorul în grupul docker
sudo usermod -aG docker $USER
# Rulați următoarea comandă sau Deconectați-vă și conectați-vă din nou și rulați (aceasta nu funcționează, poate fi necesar să reporniți mai întâi mașina)
newgrp docker

# Notă: Dacă comanda docker-compose eșuează după instalare, verificați calea. De asemenea, puteți crea o legătură simbolică către /usr/bin sau orice alt director din calea dvs.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```