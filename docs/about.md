# Despre Proiect

## "Povestea" proiectului

Această pagină a apărut ca rezultat al unui Live Demo prezentat studenților grupei FI-181 de la Universitatea Tehnică din Moldova. Scopul seminarului în cadrul obiectului de studii "Comunicații Hibride" a fost implicarea studenților în procesul de învățare a protocoalelor DNS, HTTP/S, procedurii de achiziționare a unui domeniu, configurarea acestuia, utilizarea de Static Sites Generators și găzduirea gratuită a conținutului static. Mai târziu, domeniul urmează să fie reutilizat pentru publicarea online de tutoriale, documente și proiecte open source.